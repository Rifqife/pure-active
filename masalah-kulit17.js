(function($) {
    jQuery(document).ready(function(){
        var anchor = window.location.hash;
        if (anchor !== null)
        {
            $(anchor).click(function() {
                var id = $(anchor).attr('name');
                $(id).show('medium');
                $('html, body').animate({
                    scrollTop: $(anchor).offset().top
                }, 500);
                return false;
            });
            $(anchor).trigger('click');
            return false;
        }
    });
})(jQuery)
(function($) {
	jQuery(document).ready(function(){
        $('#masalah-jerawat').click(function() {
            var id = $(this).attr('name');
            if ($(id).css('display') === 'none')
            {
                $('.drop-down').hide('medium');
                $(id).show('medium');
            }
            else {
                $(id).hide('medium');
            }
        });
        $('#masalah-komedo').click(function() {
            var id = $(this).attr('name');
            if ($(id).css('display') === 'none')
            {
                $('.drop-down').hide('medium');
                $(id).show('medium');
            }
            else {
                $(id).hide('medium');
            }
        });
        $('ul.tabs li').click(function(){
            var tab_id = $(this).attr('data-tab');

            $('ul.tabs li').removeClass('current');
            $('.tab-content').removeClass('current');

            $(this).addClass('current');
            $('.tab-content#'+tab_id).addClass('current');
        });
    });
})(jQuery)