var anchor = window.location.hash;
(function($) {
	jQuery(document).ready(function(){
        if (anchor !== null)
        {
            $(anchor).trigger('click');
            $(anchor).click(function(){
                $('html, body').animate({
                    scrollTop: $(anchor).offset().top
                }, 500);
                return false;
            });
        }
        else
        {
            $('#masalah-jerawat').click(function() {
                var id = $('#masalah-jerawat').attr('name');
                if ($('.drop-down' + id).css('display') === 'none')
                {
                    $('.drop-down').hide('medium');
                    $('.drop-down'+ id).show('medium');
                }
                else {
                    $('.drop-down').hide('medium');
                }
            });
            $('#masalah-komedo').click(function() {
                var id = $('#masalah-komedo').attr('name');
                if ($('.drop-down' + id).css('display') === 'none')
                {
                    $('.drop-down').hide('medium');
                    $('.drop-down'+ id).show('medium');
                }
                else {
                    $('.drop-down').hide('medium');
                }
            });
            $('ul.tabs li').click(function(){
                var tab_id = $(this).attr('data-tab');

                $('ul.tabs li').removeClass('current');
                $('.tab-content').removeClass('current');

                $(this).addClass('current');
                $('.tab-content#'+tab_id).addClass('current');
            });
        }
    });
})(jQuery)