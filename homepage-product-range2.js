(function($) {
	jQuery(document).ready(function(){
    	$(".sub-product-left").click(function() {
    		if ($(".hidden-right").css("display") === "none")
    		{
    			$(".hidden-left").toggle("slide", {direction: "up"});
    		}
    		else
    		{
    			$(".hidden-right").toggle("slide", {direction: "up"});
    			$(".hidden-left").toggle("slide", {direction: "up"});
    		}
		});
		$(".sub-product-right").click(function() {
    		if ($(".hidden-left").css("display") === "none")
    		{
    			$(".hidden-right").toggle("slide", {direction: "up"});
    		}
    		else
    		{
    			$(".hidden-left").toggle("slide", {direction: "up"});
    			$(".hidden-right").toggle("slide", {direction: "up"});
    		}
		});
	});
})(jQuery)