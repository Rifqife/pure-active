(function($) {
	jQuery(document).ready(function(){
    	$(".sub-product-left").click(function() {
    		if ($(".hidden-right").css("display") === "none")
    		{
    			$(".hidden-left").slideToggle();
    		}
    		else
    		{
    			$(".hidden-right").slideToggle();
    			$(".hidden-left").slideToggle();
    		}
		});
		$(".sub-product-right").click(function() {
    		if ($(".hidden-left").css("display") === "none")
    		{
    			$(".hidden-right").slideToggle();
    		}
    		else
    		{
    			$(".hidden-left").slideToggle();
    			$(".hidden-right").slideToggle();
    		}
		});
	});
})(jQuery)