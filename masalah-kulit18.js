(function($) {
	jQuery(document).ready(function(){
        var anchor = window.location.hash;
        if (anchor !== null)
        {
            if (anchor === '#trigger_masalah_komedo')
            {
                $('#masalah-komedo').one('click', function() {
                    var id = $('#masalah-komedo').attr('name');
                    $(id).show('medium');
                    $('html, body').animate({
                        scrollTop: $('#masalah-komedo').offset().top
                    }, 500);
                    return false;
                });
                $('#masalah-komedo').trigger('click');
            }
            else
            {
                $('#masalah-jerawat').one('click', function() {
                    var id = $('#masalah-jerawat').attr('name');
                    $(id).show('medium');
                    $('html, body').animate({
                        scrollTop: $('#masalah-jerawat').offset().top
                    }, 500);
                    return false;
                });
                $('#masalah-jerawat').trigger('click');
            }
        }
        $('#masalah-jerawat').click(function() {
            var id = $(this).attr('name');
            if ($(id).css('display') === 'none')
            {
                $('.drop-down').hide('medium');
                $(id).show('medium');
            }
            else {
                $(id).hide('medium');
            }
        });
        $('#masalah-komedo').click(function() {
            var id = $(this).attr('name');
            if ($(id).css('display') === 'none')
            {
                $('.drop-down').hide('medium');
                $(id).show('medium');
            }
            else {
                $(id).hide('medium');
            }
        });
        $('ul.tabs li').click(function(){
            var tab_id = $(this).attr('data-tab');

            $('ul.tabs li').removeClass('current');
            $('.tab-content').removeClass('current');

            $(this).addClass('current');
            $('.tab-content#'+tab_id).addClass('current');
        });
    });
})(jQuery)