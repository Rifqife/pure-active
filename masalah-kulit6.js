(function($) {
	jQuery(document).ready(function(){
    	$("#masalah-jerawat").click(function() {
    		var id = $('#masalah-jerawat').attr('name');
    		$(".drop-down").hide('medium');
            $(".drop-down"+ id).show("medium");
		});
		$("#masalah-komedo").click(function() {
    		var id = $('#masalah-komedo').attr('name');
    		$(".drop-down").hide('medium');
            $(".drop-down"+ id).show("medium");
		});
        $('ul.tabs li').click(function(){
            var tab_id = $(this).attr('data-tab');

            $('ul.tabs li').removeClass('current');
            $('.tab-content').removeClass('current');

            $(this).addClass('current');
            $('.tab-content#'+tab_id).addClass('current');
        });
	});
})(jQuery)