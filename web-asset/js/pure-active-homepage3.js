if (!window['YT']) {var YT = {loading: 0,loaded: 0};}if (!window['YTConfig']) {var YTConfig = {'host': 'http://www.youtube.com'};}if (!YT.loading) {YT.loading = 1;(function(){var l = [];YT.ready = function(f) {if (YT.loaded) {f();} else {l.push(f);}};window.onYTReady = function() {YT.loaded = 1;for (var i = 0; i < l.length; i++) {try {l[i]();} catch (e) {}}};YT.setConfig = function(c) {for (var k in c) {if (c.hasOwnProperty(k)) {YTConfig[k] = c[k];}}};var a = document.createElement('script');a.type = 'text/javascript';a.id = 'www-widgetapi-script';a.src = 'https://s.ytimg.com/yts/jsbin/www-widgetapi-vflnzpyZ4/www-widgetapi.js';a.async = true;var b = document.getElementsByTagName('script')[0];b.parentNode.insertBefore(a, b);})();}
(function($) {
	jQuery(document).ready(function(){
    	$('.pure-active-sensitive').click(function(){
            if ($('.sensitive-drop-down').css('display') == 'none')
            {
                $('.hidden').slideUp();
                $('.sensitive-drop-down').slideDown();
            }
            else
            {
                $('.sensitive-drop-down').slideUp();
            }
        });
        $('.pure-active-acne').click(function(){
            if ($('.acne-drop-down').css('display') == 'none')
            {
                $('.hidden').slideUp();
                $('.acne-drop-down').slideDown();
            }
            else
            {
                $('.acne-drop-down').slideUp();
            }
        });
        $('.pure-active-matcha').click(function(){
            if ($('.matcha-drop-down').css('display') == 'none')
            {
                $('.hidden').slideUp();
                $('.matcha-drop-down').slideDown();
            }
            else
            {
                $('.matcha-drop-down').slideUp();
            }
        });
        $('.tvc-thumbnail').click(function(){
            $('iframe[src*="//www.youtube.com/embed/"]').each(function(i) {
                this.contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
            });
        });
        $('.thumbnail-1').click(function(){
            $('.tvc').hide();
            $('.tvc-1').show();
        });
        $('.thumbnail-2').click(function(){
            $('.tvc').hide();
            $('.tvc-2').show();
        });
        $('.thumbnail-3').click(function(){
            $('.tvc').hide();
            $('.tvc-3').show();
        });
	});
})(jQuery)