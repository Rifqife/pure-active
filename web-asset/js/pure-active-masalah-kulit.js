(function($) {
	jQuery(document).ready(function(){
    	$('.sensitif-dan-berjerawat').click(function(){
            if ($('.sensitif-container').css('display') == 'none')
            {
                $('.drop-down').slideUp();
                $('.sensitif-container').slideDown();
            }
            else
            {
                $('.sensitif-container').slideUp();
            }
        });
        $('.masalah-jerawat').click(function(){
            if ($('.jerawat-container').css('display') == 'none')
            {
                $('.drop-down').slideUp();
                $('.jerawat-container').slideDown();
            }
            else
            {
                $('.jerawat-container').slideUp();
            }
        });
        $('.masalah-komedo').click(function(){
            if ($('.minyak-container').css('display') == 'none')
            {
                $('.drop-down').slideUp();
                $('.minyak-container').slideDown();
            }
            else
            {
                $('.minyak-container').slideUp();
            }
        });
	});
})(jQuery)