(function($) {
	jQuery(document).ready(function(){
    	$(".mitos-card-first-row").click(function() {
    		$(".hidden-first-row").hide("fast");
            $("."+this.name).show("fast");
		});
        $(".mitos-card-second-row").click(function() {
            $(".hidden-second-row").hide("fast");
            $("."+this.name).show("fast");
        });
        $(".mitos-card-third-row").click(function() {
            $(".hidden-third-row").hide("fast");
            $("."+this.name).show("fast");
        });
        $(".mitos-card-fourth-row").click(function() {
            $(".hidden-fourth-row").hide("fast");
            $("."+this.name).show("fast");
        });
	});
})(jQuery)