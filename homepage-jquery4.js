(function($) {
	jQuery(document).ready(function(){
    	$('.sub-product-left').click(function() {
    		if ($('.matcha').css('display') === 'none')
    		{
    			$('.acne-white').toggle('medium');
    		}
    		else
    		{
    			$('.matcha').toggle('medium');
    			$('.acne-white').toggle('medium');
    		}
		});
		$('.sub-product-right').click(function() {
    		if ($('.acne-white').css('display') === 'none')
    		{
    			$('.matcha').toggle('medium');
    		}
    		else
    		{
    			$('.acne-white').toggle('medium');
    			$('.matcha').toggle('medium');
    		}
		});
        $('ul.tabs li').click(function(){
            var tab_id = $(this).attr('data-tab');

            $('ul.tabs li').removeClass('current');
            $('.tab-content').removeClass('current');

            $(this).addClass('current');
            $('.tab-content#'+tab_id).addClass('current');
        });
	});
})(jQuery)