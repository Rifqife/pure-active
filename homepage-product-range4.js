(function($) {
	jQuery(document).ready(function(){
    	$(".sub-product-left").click(function() {
    		if ($(".hidden.matcha").css("display") === "none")
    		{
    			$(".hidden.acne-white").toggle("medium");
    		}
    		else
    		{
    			$(".hidden.matcha").toggle("medium");
    			$(".hidden.acne-white")toggle("medium");
    		}
		});
		$(".sub-product-right").click(function() {
    		if ($(".hidden.acne-white").css("display") === "none")
    		{
    			$(".hidden.matcha").toggle("medium");
    		}
    		else
    		{
    			$(".hidden.acne-white").toggle("medium");
    			$(".hidden.matcha")toggle("medium");
    		}
		});
	});
})(jQuery)