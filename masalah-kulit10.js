var anchor = window.location.hash;
(function($) {
	jQuery(document).ready(function(){
        if (anchor !== null)
        {
            if (anchor === '#masalah-komedo') {
                $('#masalah-komedo').click(function(){
                    $('html, body').animate({
                        scrollTop: $('#masalah-komedo').offset().top
                    }, 500);
                    return false;
                });
                $('#masalah-komedo').trigger('click');
            }
            else {
                $('#masalah-jerawat').click(function(){
                    $('html, body').animate({
                        scrollTop: $('#masalah-jerawat').offset().top
                    }, 500);
                    return false;
                });
                $('#masalah-jerawat').trigger('click');
            }
        }
        $('#masalah-jerawat').click(function() {
            var id = $('#masalah-jerawat').attr('name');
            if ($('.drop-down' + id).css('display') === 'none')
            {
                $('.drop-down').hide('medium');
                $('.drop-down'+ id).show('medium');
            }
            else {
                $('.drop-down').hide('medium');
            }
        });
        $('#masalah-komedo').click(function() {
            var id = $('#masalah-komedo').attr('name');
            if ($('.drop-down' + id).css('display') === 'none')
            {
                $('.drop-down').hide('medium');
                $('.drop-down'+ id).show('medium');
            }
            else {
                $('.drop-down').hide('medium');
            }
        });
        $('ul.tabs li').click(function(){
            var tab_id = $(this).attr('data-tab');

            $('ul.tabs li').removeClass('current');
            $('.tab-content').removeClass('current');

            $(this).addClass('current');
            $('.tab-content#'+tab_id).addClass('current');
        });
    });
})(jQuery)