(function($) {
	jQuery(document).ready(function(){
        $('a.card-link1').click(function(e){
            e.preventDefault();
            var id = $(this).attr('href');
            if ($('.hidden-first-row' + id).css('display') === 'none')
            {
                $('.hidden-first-row').hide('medium');
                $('.hidden-first-row' + id).show('medium');
            }
            else
            {
                $('.hidden-first-row').hide('medium');
            }
        });
        $('a.card-link2').click(function(e){
            e.preventDefault();
            var id = $(this).attr('href');
            if ($('.hidden-second-row' + id).css('display') === 'none')
            {
                $('.hidden-second-row').hide('medium');
                $('.hidden-second-row' + id).show('medium');
            }
            else
            {
                $('.hidden-second-row').hide('medium');
            }
        });
        $('a.card-link3').click(function(e){
            e.preventDefault();
            var id = $(this).attr('href');
            if ($('.hidden-third-row' + id).css('display') === 'none')
            {
                $('.hidden-third-row').hide('medium');
                $('.hidden-third-row' + id).show('medium');
            }
            else
            {
                $('.hidden-third-row').hide('medium');
            }
        });
        $('a.card-link4').click(function(e){
            e.preventDefault();
            var id = $(this).attr('href');
            if ($('.hidden-fourth-row' + id).css('display') === 'none')
            {
                $('.hidden-fourth-row').hide('medium');
                $('.hidden-fourth-row' + id).show('medium');
            }
            else
            {
                $('.hidden-fourth-row').hide('medium');
            }
        });
    });
})(jQuery)